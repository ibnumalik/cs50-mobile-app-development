const classNames = {
  TODO_ITEM: 'todo-container',
  TODO_CHECKBOX: 'todo-checkbox',
  TODO_TEXT: 'todo-text',
  TODO_DELETE: 'todo-delete',
}

const list = document.getElementById('todo-list')
const itemCountSpan = document.getElementById('item-count')
const uncheckedCountSpan = document.getElementById('unchecked-count')

const todoLists = get('todo') || []
let todoId = 0
let uncheckedCount = 0

init()

function init() {
  todoLists.forEach(todo => {
    if (todo.status === 'ongoing') {
      uncheckedCount = uncheckedCount + 1
    }

    todoId = todo.id > todoId ? todo.id : todoId

    updateView(todo)
  })

  todoId++
}

function newTodo() {
  let userTodo = prompt('What do you want to do?')

  if (!userTodo) {
    return newTodo()
  }

  const newTodoItem = {
    id: todoId,
    title: userTodo,
    status: 'ongoing'
  }
  todoLists.push(newTodoItem)

  todoId = todoId + 1
  uncheckedCount = uncheckedCount + 1
  updateView(newTodoItem)
  save('todo', todoLists)
}

function updateView(todo) {
  list.insertAdjacentHTML('afterbegin', `
    <li class="${classNames.TODO_ITEM}" >
      <input
        onclick="toggleTodo(this)"
        type="checkbox"
        class="${classNames.TODO_CHECKBOX}"
        data-id="${todo.id}"
        ${isChecked(todo)}>
      <span class="${classNames.TODO_TEXT} ${isDone(todo)}">
        ${todo.title}
      </span>
      <button class="delete" onclick="deleteTodo(this)">delete</button>
    </li>
  `)

  itemCountSpan.innerHTML = todoLists.length
  uncheckedCountSpan.innerHTML = uncheckedCount
}

function toggleTodo(element) {
  let todoId = element.attributes['data-id'].value
  let [currentTodo] = todoLists.filter(todo => todo.id == todoId)

  toggleClass(element.nextElementSibling, 'done')

  if (currentTodo.status === 'ongoing') {
    currentTodo.status = 'done'
    uncheckedCount = uncheckedCount - 1
  } else {
    currentTodo.status = 'ongoing'
    uncheckedCount = uncheckedCount + 1
  }

  uncheckedCountSpan.innerHTML = uncheckedCount
  save('todo', todoLists)
}

function deleteTodo(el) {
  let toDelete
  let todoId = el.parentElement.firstElementChild.attributes['data-id'].value

  todoLists.filter((todo, index) => {
    if(todo.id == todoId) {
      toDelete = index
    }
  })

  todoLists.splice(toDelete, 1)

  el.parentElement.remove()
  save('todo', todoLists)

  itemCountSpan.innerHTML = todoLists.length
  uncheckedCountSpan.innerHTML = countUnchecked()

}

function isDone(todo) {
  return todo.status === 'done' ? 'done' : ''
}

function isChecked(todo) {
  return todo.status === 'done' ? 'checked': ''
}

function countUnchecked() {
  let unchecked = 0
  todoLists.forEach((el) => {
     if(el.status === 'ongoing') {
       unchecked = unchecked + 1
     }
  })

  return unchecked
}

/**
 * HTML class manipulation
 */

function hasClass(elem, className) {
  return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}

function toggleClass(elem, className) {
  var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ' ) + ' ';
  if (hasClass(elem, className)) {
      while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
          newClass = newClass.replace( ' ' + className + ' ' , ' ' );
      }
      elem.className = newClass.replace(/^\s+|\s+$/g, '');
  } else {
      elem.className += ' ' + className;
  }
}

function save(key, items) {
  let jsonItem;

  if (isObject(items)) {
    jsonItem = JSON.stringify(items)
  }

  localStorage.setItem(key, jsonItem)
}

function get(key) {
  let items = localStorage.getItem(key)

  try {
    return JSON.parse(items)
  } catch(err) {
    return items
  }
}

function isObject(item) {
  return typeof item !== null && typeof item === 'object'
}